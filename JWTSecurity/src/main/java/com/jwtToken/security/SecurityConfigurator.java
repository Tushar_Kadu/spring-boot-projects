package com.jwtToken.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.jwtToken.components.JwtAuthenticationEntryPoint;
import com.jwtToken.filters.JwtRequestFilter;
import com.jwtToken.services.MyUserDetailsService;


@SuppressWarnings("deprecation")
@EnableWebSecurity
public class SecurityConfigurator extends WebSecurityConfigurerAdapter{
	
	@Autowired
	MyUserDetailsService myUserDetailsService;
	
	@Autowired
	JwtRequestFilter jwtRequestFilters;
	
	@Autowired
	JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		// Authenticate the userdetails username and password
		auth.userDetailsService(myUserDetailsService);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// Don't use authentication when we are using Authentication service
		// For all other request use web security mechanism
		http.csrf().disable()
			.authorizeRequests()
			.antMatchers("/authenticate","/addUser").permitAll()
			.antMatchers("/findUserByName/**","/getAllUsers").hasRole("ADMIN")
			.antMatchers("/getUserById/**","/HelloJWTDemo").hasRole("USER")
			.anyRequest().authenticated()
			.and()
			.exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint)
			.and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
			// Last line is for JWT token :- Hey spring security dont manage the sessions
		
		http.addFilterBefore(jwtRequestFilters, UsernamePasswordAuthenticationFilter.class);
	}
	
	@Bean
	public PasswordEncoder passwordEncoder(){
		return NoOpPasswordEncoder.getInstance();
	}
	
	@Bean
	public AuthenticationManager authenticationManagerBean()throws Exception{
		return super.authenticationManager();
	}
}
