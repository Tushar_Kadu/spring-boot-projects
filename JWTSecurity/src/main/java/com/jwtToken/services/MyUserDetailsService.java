package com.jwtToken.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jwtToken.models.Role;
import com.jwtToken.models.User;
import com.jwtToken.repository.RoleRepository;
import com.jwtToken.repository.UserRepository;


@Service
public class MyUserDetailsService implements UserDetailsService{

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	

	
	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		 
		User user = userRepository.findByUserName(userName);
		
		// Use core user for the authentication and other details
		org.springframework.security.core.userdetails.User coreUser = new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(), getAuthorities(user));
		//org.springframework.security.core.userdetails.User coreUser = new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(), new ArrayList<>());
		return coreUser;
	}
	
	/**
	 * Get all the authorities of the users (like ADMIN/USER)
	 * @param user
	 * @return
	 */
	private static Collection<? extends GrantedAuthority> getAuthorities(User user) {
        String[] userRoles = user.getRoles().stream().map((role) -> role.getName()).toArray(String[]::new);
        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(userRoles);
        return authorities;
    }
	
	public @ResponseBody List<User> getAllUsers(){
		
		List<User> users = new ArrayList<>();
		userRepository.findAll().forEach(users::add);		
		return users;
	}
	
	public User addUser(User user) {
		
		// We dont need to create new role every time if role is already exist we need to use existing reference
		List<Role> userRoles = user.getRoles();
		
		// Iterate on roles nad set existing roleIds
		for (Role role : userRoles) {
			
			Role existingRole = roleRepository.findByRoleName(role.getName());
			
			if(null != existingRole)
				role.setRoleId(existingRole.getRoleId());
		}
		
		// Save user details
		return userRepository.save(user);
	}
	
	public Optional<User> findUser(int userId) {
		
		return userRepository.findById(userId);
		
	}

	public User findByUserName(String userName) {
		
		return userRepository.findByUserName(userName);
		
	}

}
