package com.jwtToken.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Class to hold the user Role details
 *  
 * @author GS-1157
 *
 */
@Entity
@Table(name = "my_roles")
public class Role {

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="role_id")
	private int roleId;
	
	@Column(name="role_name")
	private String roleName;
	
	@ManyToMany(mappedBy = "roles")
    private List < User > my_user = new ArrayList<>();
	
	public Role()
	{
		
	}
	
	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getName() {
		return roleName;
	}

	public void setName(String name) {
		this.roleName = name;
	}

	@JsonIgnore
	public List<User> getMy_user() {
		return my_user;
	}

	public void setMy_user(List<User> my_user) {
		this.my_user = my_user;
	}
	
}
