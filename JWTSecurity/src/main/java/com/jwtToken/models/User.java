package com.jwtToken.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import com.jwtToken.models.Role;
import javax.persistence.JoinColumn;

/**
 * Class to hold the User details
 * @author GS-1157
 *
 */
@Entity
@Table(name = "my_user")
public class User {

	
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="user_id")
	int userId;
	@Column(name="user_name")
	String userName;
	@Column(name="user_pass")
	String password;
	@Column(name="user_add")
	String address;
	
	@ManyToMany(cascade=CascadeType.MERGE)
    @JoinTable(
    		 name="user_role",
    	       joinColumns={@JoinColumn(name="USER_ID", referencedColumnName="user_id")},
    	       inverseJoinColumns={@JoinColumn(name="ROLE_ID", referencedColumnName="role_Id")}
    		 )

    private List<Role> roles = new ArrayList<Role>();

	
	public User()
	{
		
	}
	
	public User(int userId, String userName, String password, String address) {
		this.userId = userId;
		this.userName = userName;
		this.password = password;
		this.address = address;
	}
		
	public int getUserId() {
		return userId;
	}
	
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
			
	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	public void clearRoles()
	{
		this.roles = null;
	}

}
