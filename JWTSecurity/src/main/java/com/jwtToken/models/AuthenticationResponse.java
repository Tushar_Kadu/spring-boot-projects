package com.jwtToken.models;

/**
 * Class to hold the JWT token details
 * @author GS-1157
 *
 */
public class AuthenticationResponse {

	private final String jwt;
	
	public AuthenticationResponse(String token)
	{
		this.jwt=token;
	}
	
	public String getJwt()
	{
		return this.jwt;
	}
}
