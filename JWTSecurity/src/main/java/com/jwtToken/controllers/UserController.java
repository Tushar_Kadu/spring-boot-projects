package com.jwtToken.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jwtToken.models.User;
import com.jwtToken.services.MyUserDetailsService;

/**
 * Controller for 
 * 1. Create User
 * 2. Find user by id
 * 3. Find user by name
 * 4. Find all users
 * 
 * @author GS-1157
 *
 */

@RestController
public class UserController {

	@Autowired
	MyUserDetailsService userService;
	
	@RequestMapping(method=RequestMethod.POST, value="/getAllUsers")
	public List<User> getAllUsers() {
		
		return userService.getAllUsers();
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/addUser")
	public @ResponseBody User addUser(@RequestBody User user ) throws Exception{
		
		 return userService.addUser(user);
		 
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/getUserById/{userId}")
	public @ResponseBody Optional<User> findUserById(@PathVariable int userId)
	{
		return userService.findUser(userId);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/findUserByName/{username}")
	public @ResponseBody User findUserByName(@PathVariable String username) throws Exception{
		
		 return userService.findByUserName(username);
		 
	}

}