package com.jwtToken.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jwtToken.models.Role;
import com.jwtToken.services.RoleService;
/**
 * Controller to 
 * 1. Create the role
 * 2. Find role by name
 * 
 * @author GS-1157
 *
 */
@RestController
public class RoleController {

	@Autowired
	RoleService roleService;
	
	@RequestMapping(method=RequestMethod.POST, value="/addRole")
	public @ResponseBody Role addUser(@RequestBody Role role ) throws Exception{
		
		 return roleService.addRole(role);
		 
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/findRoleByName/{username}")
	public @ResponseBody Role findUserByName(@PathVariable String roleName) throws Exception{
		
		 return roleService.findRoleByName(roleName);
		 
	}

}
