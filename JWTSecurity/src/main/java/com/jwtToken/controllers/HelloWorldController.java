package com.jwtToken.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

	@RequestMapping({"/HelloJWTDemo"})
	public String helloWorld()
	{
		return "<H1> Hello INDIA </H1>";
	}
	
}
