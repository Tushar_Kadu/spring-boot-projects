package com.jwtToken.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jwtToken.models.AuthenticationRequest;
import com.jwtToken.models.AuthenticationResponse;
import com.jwtToken.services.MyUserDetailsService;
import com.jwtToken.utilities.JWTUtils;
/**
 * This class is used for the 
 * 1. Authenticate User
 * 2. Create the JWT Token and return it
 * 
 * @author GS-1157
 *
 */
@RestController
public class AuthenticationController {

	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	MyUserDetailsService userDetailsservice;
	
	@Autowired
	JWTUtils JWTUtils;
	
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authRequest) throws Exception
	{
		
		// Authenticate user
		try {
			
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(authRequest.getUserName() ,authRequest.getPassword())
					);	
		} catch (Exception e) {
			// If authentication failed we need to throw and exception
			throw new Exception("Incorrect username or password", e);
		}
		
		// Authentication is successful now you need to generate the Token
		
		// Take user details
		final UserDetails userDetails = userDetailsservice.loadUserByUsername(authRequest.getUserName()); 
		
		// Create Access Token using utils
		final String jwtToken = JWTUtils.generateToken(userDetails);
		
		// Return Token
		return ResponseEntity.ok(new AuthenticationResponse(jwtToken));
	}
}
