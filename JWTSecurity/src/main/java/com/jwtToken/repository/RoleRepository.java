package com.jwtToken.repository;
import org.springframework.data.repository.CrudRepository;

import com.jwtToken.models.Role;


public interface RoleRepository extends CrudRepository<Role,Integer> {

	public Role findByRoleName(String roleName);
}
