package com.jwtToken.repository;

import org.springframework.data.repository.CrudRepository;

import com.jwtToken.models.User;


public interface UserRepository extends CrudRepository<User, Integer> {
	
	public User findByUserName(String userName);
}
