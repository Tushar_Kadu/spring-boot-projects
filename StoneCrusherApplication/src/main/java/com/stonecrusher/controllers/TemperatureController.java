package com.stonecrusher.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stonecrusher.common.ApiError;
import com.stonecrusher.models.Temperature;
import com.stonecrusher.services.TemperatureService;

@RestController
@RequestMapping(value = "/api/v1")
public class TemperatureController {

	public static final Logger logger = LogManager.getLogger(TemperatureController.class); 
	@Autowired
	TemperatureService temperatureService;
	
	@RequestMapping(method=RequestMethod.POST, value="/saveTemperature")
	public ResponseEntity<?> saveTemperatureDetails(@RequestBody Temperature temperature){
	
		logger.info("Creating new Temperature ");
		if(temperatureService.findTemperature(temperature.getTempSensorName())  == null) {			
			return new ResponseEntity<>(temperatureService.saveTemperature(temperature),HttpStatus.CREATED);
		}
		else
		{
			logger.info("Temperature sensor " + temperature.getTempSensorName() + " is already exist ");
			return new ResponseEntity<ApiError>(new ApiError(HttpStatus.ALREADY_REPORTED,"Temperature sensor already exist"),HttpStatus.ALREADY_REPORTED);
		}
		 
	}
		
}
