package com.stonecrusher.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.stonecrusher.models.Load;
import com.stonecrusher.services.LoadService;

@RestController
@RequestMapping(value = "/api/v1")
public class LoadController {

	public static final Logger logger = LogManager.getLogger(LoadController.class); 
	
	@Autowired
	LoadService loadService;
	
	@RequestMapping(method=RequestMethod.POST, value="/saveLoad")
	public ResponseEntity<?> saveLoadDetails(@RequestBody Load load){
		
		logger.info("Creating load details ");
		return new ResponseEntity<>(loadService.saveLoad(load),HttpStatus.CREATED);
	}
}
