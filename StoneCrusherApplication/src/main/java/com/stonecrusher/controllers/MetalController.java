package com.stonecrusher.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.stonecrusher.common.ApiError;
import com.stonecrusher.models.Metal;
import com.stonecrusher.services.MetalService;

@RestController
@RequestMapping(value = "/api/v1")
public class MetalController {

	public static final Logger logger = LogManager.getLogger(MetalController.class); 
	@Autowired
	MetalService metalService;
	
	@RequestMapping(method=RequestMethod.POST, value="/saveMetal")
	public ResponseEntity<?> saveMetalDetails(@RequestBody Metal metal){
	
		logger.info("Creating new Metal");
		if(metalService.findMetal(metal.getMetalName())  == null) {			
			return new ResponseEntity<>(metalService.saveMetal(metal),HttpStatus.CREATED);
		}
		else
		{
			logger.info("Metal " + metal.getMetalName() + " is already exist ");
			return new ResponseEntity<ApiError>(new ApiError(HttpStatus.ALREADY_REPORTED,"Metal already exist"),HttpStatus.ALREADY_REPORTED);
		}
		 
	}
		
}
