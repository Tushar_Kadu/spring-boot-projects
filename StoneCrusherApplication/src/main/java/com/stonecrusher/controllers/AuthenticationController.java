package com.stonecrusher.controllers;

import java.net.HttpURLConnection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stonecrusher.common.ApiError;
import com.stonecrusher.models.AuthenticationRequest;
import com.stonecrusher.models.AuthenticationResponse;
import com.stonecrusher.services.UserService;
import com.stonecrusher.utilities.JWTUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
/**
 * This class is used for the 
 * 1. Authenticate User
 * 2. Create the JWT Token and return it
 * 
 * @author GS-1157
 *
 */
@RestController
@Api(tags = "Authentication", value = "Authentication API which gives JWT token on success")
@RequestMapping(value = "/api/v1")
public class AuthenticationController {

	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	UserService userDetailsservice;
	
	@Autowired
	JWTUtils JWTUtils;
	
	@CrossOrigin
	@ApiOperation(value = "Authenticate user")
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Report", response =AuthenticationResponse.class),
			@ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Auth Failed", response = ApiError.class),
			@ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Request failed", response = ApiError.class) })

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authRequest) throws Exception
	{		
		// Authenticate user
		try {
			
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(authRequest.getUserName() ,authRequest.getPassword())
					);	
		} catch (Exception e) {
			// If authentication failed we need to throw and exception
			//throw new  ProcessCustomError("Invalid username or password ", HttpStatus.NOT_FOUND);
			throw new Exception("Invalid username or password",e);
		}
		
		// Authentication is successful now you need to generate the Token
		
		// Take user details
		final UserDetails userDetails = userDetailsservice.loadUserByUsername(authRequest.getUserName()); 
		
		// Create Access Token using utils
		final String jwtToken = JWTUtils.generateToken(userDetails);
		
		// Update User pushnotification token
		
		// Return Token
		return ResponseEntity.ok(new AuthenticationResponse(jwtToken,"SUCCESS"));
	}
}
