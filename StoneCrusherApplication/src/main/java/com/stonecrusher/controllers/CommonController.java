package com.stonecrusher.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stonecrusher.exception.ProcessCustomError;
import com.stonecrusher.utilities.SendPushNotification;

@RestController
@RequestMapping(value = "/api/v1")
public class CommonController {

	public static final Logger logger = LogManager.getLogger(CommonController.class);
	
	@Autowired
	SendPushNotification sendPushNotification;
	
	@RequestMapping(method=RequestMethod.POST, value="/sendNotification")
	public ResponseEntity<?> sendPushNotification() throws ProcessCustomError
	{	
		return new ResponseEntity<>(sendPushNotification.sendPushNotification(), HttpStatus.OK);
	}

}
