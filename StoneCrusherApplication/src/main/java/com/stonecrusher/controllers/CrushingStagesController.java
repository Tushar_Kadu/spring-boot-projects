package com.stonecrusher.controllers;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stonecrusher.common.ApiError;
import com.stonecrusher.models.CrushingStages;
import com.stonecrusher.services.CrushingStagesService;

@RestController
@RequestMapping(value = "/api/v1")
public class CrushingStagesController {

	private static final Logger LOGGER = LogManager.getLogger(CrushingStagesController.class);
	
	@Autowired
	CrushingStagesService crushingStagesService;
	
	@RequestMapping(method=RequestMethod.POST, value="/saveCrushingStage")
	public ResponseEntity<?> saveCrushingStage(@RequestBody CrushingStages stage ) throws Exception{

		LOGGER.info("Adding new crushing stage");
		
		if(crushingStagesService.findCrushingStage(stage.getStageName()) == null)
		{
			CrushingStages newStage = crushingStagesService.saveCrushingStage(stage);
			return new ResponseEntity<>(newStage, HttpStatus.CREATED);		
		}
		else{
			LOGGER.info("CrusherStage "+ stage.getStageName() + " already exist");
			return new ResponseEntity<ApiError>(new ApiError(HttpStatus.ALREADY_REPORTED,"Crushing stage already exist"),HttpStatus.ALREADY_REPORTED);
		}
					 
	}

	@RequestMapping(method=RequestMethod.GET, value="/getCrushingStages")
	public ResponseEntity<?> getCrushingStages() throws Exception{

		LOGGER.info("Fetching all crushing stages");
		
		List<CrushingStages> stagesList = crushingStagesService.findAllCrushingStages();
		return new ResponseEntity<>(stagesList, HttpStatus.OK);		
							 
	}

	@RequestMapping(method=RequestMethod.GET, value="/getCrushingStageByName/{stagename}")
	public ResponseEntity<?> getSpecificCrushingStage(@PathVariable String stageName) throws Exception{

		LOGGER.info("Fetching all crushing stages");
		
		CrushingStages stage = crushingStagesService.findCrushingStage(stageName);
		
		if(stage != null) 
			return new ResponseEntity<>(stage, HttpStatus.OK);		
		else
			return new ResponseEntity<ApiError>(new ApiError(HttpStatus.NOT_FOUND,"Crushing stage not found"),HttpStatus.NOT_FOUND);
							 
	}

}
