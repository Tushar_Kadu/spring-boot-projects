package com.stonecrusher.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stonecrusher.common.ApiError;
import com.stonecrusher.models.Belt;
import com.stonecrusher.services.BeltService;

@RestController
@RequestMapping(value = "/api/v1")
public class BeltController {

	public static final Logger logger = LogManager.getLogger(BeltController.class); 
	@Autowired
	BeltService beltService;
	
	@RequestMapping(method=RequestMethod.POST, value="/saveBelt")
	public ResponseEntity<?> saveBeltDetails(@RequestBody Belt belt){
	
		logger.info("Creating new Belt");
		if(beltService.findBelt(belt.getBeltName())  == null) {			
			return new ResponseEntity<>(beltService.saveBelt(belt),HttpStatus.CREATED);
		}
		else
		{
			logger.info("Belt " + belt.getBeltName() + " is already exist ");
			return new ResponseEntity<ApiError>(new ApiError(HttpStatus.ALREADY_REPORTED,"Belt already exist"),HttpStatus.ALREADY_REPORTED);
		}
		 
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/getAllBelts")
	public ResponseEntity<?> getBeltDetails(){
	
		// Find all belt details		
		return new ResponseEntity<>(beltService.findAllBelts(),HttpStatus.OK);		
	}
}
