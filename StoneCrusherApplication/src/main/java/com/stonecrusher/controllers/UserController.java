package com.stonecrusher.controllers;

import java.util.Optional;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.stonecrusher.common.ApiError;
import com.stonecrusher.models.User;
import com.stonecrusher.request.ResetPasswordRequest;
import com.stonecrusher.services.UserService;
import com.stonecrusher.utilities.SendPushNotification;

/**
 * Controller for 
 * 1. Create User
 * 2. Find user by id
 * 3. Find user by name
 * 4. Find all users
 * 
 * @author GS-1157
 *
 */

@RestController
@RequestMapping(value = "/api/v1")
public class UserController {
	
	private static final Logger LOGGER = LogManager.getLogger(UserController.class);
	
	@Autowired
	UserService userService;
	
	@RequestMapping(method=RequestMethod.GET, value="/getAllUsers")
	public ResponseEntity<?> getAllUsers() {
		
		return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/registerUser")
	public ResponseEntity<?> addUser(@RequestBody User user ) throws Exception{
		LOGGER.info("Adding new user");
		if(userService.findByUserName(user.getUserName()) == null)
		{
			User newUser = userService.addUser(user);
			return new ResponseEntity<>(newUser, HttpStatus.CREATED);		
		}
		else{
			LOGGER.info("User "+ user.getUserName() + " already exist");
			return new ResponseEntity<ApiError>(new ApiError(HttpStatus.ALREADY_REPORTED,"User already exist"),HttpStatus.ALREADY_REPORTED);
		}
							 
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/resetPassword")
	public ResponseEntity<?> addUser(@RequestBody ResetPasswordRequest passwordChangeReq ) throws Exception{
		LOGGER.info("Changing password of  user " + passwordChangeReq.getUserName());
		
		if(userService.findByUserName(passwordChangeReq.getUserName()) != null)
		{
			if(!(passwordChangeReq.getNewPassword().equalsIgnoreCase(passwordChangeReq.getConfirmPassword())))
			{
				LOGGER.info(" New password and confirm password is not same");
				return new ResponseEntity<ApiError>(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR," New password and confirm password is not same"),HttpStatus.INTERNAL_SERVER_ERROR);				
			}
			
			User newUser = userService.resetPassword(passwordChangeReq);
			return new ResponseEntity<>(newUser, HttpStatus.CREATED);		
		}
		else{
			LOGGER.info("User "+ passwordChangeReq.getUserName() + " does not exist");
			return new ResponseEntity<ApiError>(new ApiError(HttpStatus.NOT_FOUND,"User does not exist"),HttpStatus.NOT_FOUND);
		}
			
				 
	}

	
	@RequestMapping(method=RequestMethod.GET, value="/getUserById/{userId}")
	public ResponseEntity<?> findUserById(@PathVariable int userId)
	{		
		Optional<User> user = userService.findUser(userId);
		
		if(null != user)
			return new ResponseEntity<>(user, HttpStatus.OK);		
		else{
			LOGGER.info("User with userid "+ userId + " does not exist");
			return new ResponseEntity<ApiError>(new ApiError(HttpStatus.NOT_FOUND,"User not found"),HttpStatus.NOT_FOUND);
		}
			
		
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/findUserByName/{username}")
	public ResponseEntity<?> findUserByName(@PathVariable String username) throws Exception{
		
		 User findUser = userService.findByUserName(username);
		 
		 if(null != findUser)			 
			 return new ResponseEntity<>(findUser, HttpStatus.OK);
		 else{
			 LOGGER.info("User with username "+ username + " does not exist");
			 return new ResponseEntity<ApiError>(new ApiError(HttpStatus.NOT_FOUND,"User not found"),HttpStatus.NOT_FOUND);
		 }
			 
		
	}

}