package com.stonecrusher.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stonecrusher.common.ApiError;
import com.stonecrusher.models.Crusher;
import com.stonecrusher.services.CrusherService;

@RestController
@RequestMapping(value = "/api/v1")
public class CrusherController {

	public static final Logger logger = LogManager.getLogger(CrusherController.class); 
	@Autowired
	CrusherService crusherService;
	
	@RequestMapping(method=RequestMethod.POST, value="/saveCrusher")
	public ResponseEntity<?> saveCrusherDetails(@RequestBody Crusher crusher){
	
		logger.info("Creating new crusher");
		if(crusherService.findCrusher(crusher.getCrusherName())  == null) {			
			return new ResponseEntity<>(crusherService.saveCrusher(crusher),HttpStatus.CREATED);
		}
		else
		{
			logger.info("Crusher " + crusher.getCrusherName() + " is already exist ");
			return new ResponseEntity<ApiError>(new ApiError(HttpStatus.ALREADY_REPORTED,"Crusher already exist"),HttpStatus.ALREADY_REPORTED);
		}
		 
	}
		
}
