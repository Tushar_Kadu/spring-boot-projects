package com.stonecrusher.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.endpoint.MessageProducerSupport;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.integration.stream.CharacterStreamReadingMessageSource;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

import com.stonecrusher.StoneCrusherApplication;
import com.stonecrusher.constants.IConstantProperties;
import com.stonecrusher.services.MQTTService;

@Configuration
public class MqttConfig {

	private static final Logger LOGGER = LogManager.getLogger(StoneCrusherApplication.class);
	
	@Autowired
	MQTTService mqttMessageService; 

	@Bean
	public MqttPahoClientFactory mqttClientFactory() {
		DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
		MqttConnectOptions options = new MqttConnectOptions();
		options.setServerURIs(new String[] { IConstantProperties.MQTT_URL });
		options.setUserName(IConstantProperties.MQTT_USERNAME);
		options.setPassword(IConstantProperties.MQTT_PASSQORD.toCharArray());
		factory.setConnectionOptions(options);
		return factory;
	}
	
/*	@Bean
	public IntegrationFlow mqttOutFlow() {
		return IntegrationFlows.from(CharacterStreamReadingMessageSource.stdin(),
						e -> e.poller(Pollers.fixedDelay(1000)))
				.transform(p -> p + " sent to MQTT")
				.handle(mqttOutbound())
				.get();
	}
*/
	@Bean
	@ServiceActivator(inputChannel = "mqttOutboundChannel")
	public MessageHandler mqttOutbound() {
		MqttPahoMessageHandler messageHandler = new MqttPahoMessageHandler("siSamplePublisher", mqttClientFactory());
		messageHandler.setAsync(true);
		messageHandler.setDefaultTopic(IConstantProperties.MQTT_TOPIC);
		return messageHandler;
	}

	@Bean
    public MessageChannel mqttOutboundChannel() {
	        return new DirectChannel();
    }	
	
	// consumer
	@ServiceActivator(inputChannel = "handleMqttMsg")
    public void in(String message) throws JSONException {		
		LOGGER.info("MQTT message is " + message);
		
		// Delegate message to the service for processing
		mqttMessageService.processMQTTMessage(message);
    }
	
	@Bean
	public MessageProducerSupport mqttInbound() {
		MqttPahoMessageDrivenChannelAdapter adapter = new MqttPahoMessageDrivenChannelAdapter("siSampleConsumer",
				mqttClientFactory(), IConstantProperties.MQTT_TOPIC);
		adapter.setCompletionTimeout(5000);
		adapter.setConverter(new DefaultPahoMessageConverter());
		adapter.setQos(1);
	    adapter.setOutputChannelName("handleMqttMsg");
		return adapter;
	}
	
}
