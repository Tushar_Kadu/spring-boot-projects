package com.stonecrusher.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.stonecrusher.components.JwtAuthenticationEntryPoint;
import com.stonecrusher.filters.JwtAuthenticationFilter;
import com.stonecrusher.services.UserService;


@SuppressWarnings("deprecation")
@EnableWebSecurity
public class SecurityConfigurator extends WebSecurityConfigurerAdapter{
	
	@Autowired
	UserService myUserDetailsService;
	
	@Autowired
	JwtAuthenticationFilter jwtAuthenticationFilters;
	
	@Autowired
	JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		// Authenticate the userdetails username and password
		auth.userDetailsService(myUserDetailsService);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// Don't use authentication when we are using Authentication service
		// For all other request use web security mechanism
		http.csrf().disable()
			.authorizeRequests()
			.antMatchers("/api/v1/authenticate","/api/v1/registerUser","/api/v1/resetPassword","/api/v1/getAllBelts", "/api/v1/saveCrusher","/api/v1/saveCrushingStage", "/api/v1/saveMetal", "/api/v1/saveLoad","/api/v1/saveBelt","/api/v1/saveTemperature","/api/v1/sendNotification","/api/v1/publishMQTTMessage","/v2/api-docs","/configuration/ui","/swagger-resources/**","/configuration/security","/swagger-ui.html","/webjars/**").permitAll()
			.antMatchers("/api/v1/findUserByName/**").hasRole("ADMIN")
			.antMatchers("/api/v1/getUserById/**","/api/v1/getAllUsers","/api/v1/getCrushingStages","/api/v1/getCrushingStageByName/**").hasRole("USER")
			.anyRequest().authenticated()
			.and()
			.exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint)
			.and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
			// Last line is for JWT token :- Hey spring security dont manage the sessions
		
		
		http.addFilterBefore(jwtAuthenticationFilters, UsernamePasswordAuthenticationFilter.class);
	}
	
	@Bean
	public PasswordEncoder passwordEncoder(){
		return NoOpPasswordEncoder.getInstance();
		//return new BCryptPasswordEncoder();
	}
	
	@Bean
	public AuthenticationManager authenticationManagerBean()throws Exception{
		return super.authenticationManager();
	}
}
