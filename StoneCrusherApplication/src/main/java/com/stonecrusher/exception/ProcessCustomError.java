package com.stonecrusher.exception;

import org.springframework.http.HttpStatus;

import com.stonecrusher.common.ApiError;

public class ProcessCustomError extends Exception{

	private static final long serialVersionUID = -5137371098583567742L;
	private ApiError apierror;
	private HttpStatus status;

	public ProcessCustomError(String message,HttpStatus status) {
		super();
		apierror = new ApiError();
		apierror.setCode(Integer.parseInt(String.valueOf(status)));
		apierror.setMessage(message);
		this.status = status;
	}

	public ProcessCustomError(String msg, String error, HttpStatus status) {
		apierror = new ApiError();
		apierror.setErrorNo(error);
		apierror.setMessage(msg);
		apierror.setCode(Integer.parseInt(String.valueOf(status)));
		this.status = status;
	}
	
	public ApiError getApierror() {
		return apierror;
	}
	public void setApierror(ApiError apierror) {
		this.apierror = apierror;
	}
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	
}
