package com.stonecrusher.repository;
import org.springframework.data.repository.CrudRepository;

import com.stonecrusher.models.Role;


public interface RoleRepository extends CrudRepository<Role,Integer> {

	public Role findByRoleName(String roleName);
}
