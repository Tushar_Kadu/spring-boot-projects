package com.stonecrusher.repository;


import org.springframework.data.repository.CrudRepository;

import com.stonecrusher.models.Metal;

public interface MetalRepository  extends CrudRepository<Metal, Integer>{

	public Metal findByMetalName(String MetalName);
}
