package com.stonecrusher.repository;

import org.springframework.data.repository.CrudRepository;

import com.stonecrusher.models.CrushingStages;

public interface CrushingStagesRepository extends CrudRepository<CrushingStages, Integer>{
	
	public CrushingStages findByStageName(String crushingStages);
}
