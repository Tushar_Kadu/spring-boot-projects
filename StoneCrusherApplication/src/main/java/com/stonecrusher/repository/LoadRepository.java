package com.stonecrusher.repository;

import org.springframework.data.repository.CrudRepository;

import com.stonecrusher.models.Load;

public interface LoadRepository extends CrudRepository<Load, Integer> {
	
}
