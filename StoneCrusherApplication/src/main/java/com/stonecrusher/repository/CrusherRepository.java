package com.stonecrusher.repository;

import org.springframework.data.repository.CrudRepository;

import com.stonecrusher.models.Crusher;


public interface CrusherRepository extends CrudRepository<Crusher, Integer>{

	public Crusher findByCrusherName(String crusherName);
}
