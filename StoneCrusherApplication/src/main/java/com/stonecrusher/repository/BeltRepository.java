package com.stonecrusher.repository;

import org.springframework.data.repository.CrudRepository;

import com.stonecrusher.models.Belt;


public interface BeltRepository extends CrudRepository<Belt, Integer> {

	public Belt findByBeltName(String beltName);
}
