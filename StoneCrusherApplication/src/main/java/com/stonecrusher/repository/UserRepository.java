package com.stonecrusher.repository;

import org.springframework.data.repository.CrudRepository;

import com.stonecrusher.models.User;


public interface UserRepository extends CrudRepository<User, Integer> {
	
	public User findByUserName(String userName);
}
