package com.stonecrusher.repository;

import org.springframework.data.repository.CrudRepository;

import com.stonecrusher.models.Temperature;

public interface TemperatureRepository extends CrudRepository<Temperature, Integer> {

	public Temperature findByTempSensorName(String sensorName);
}
