package com.stonecrusher.utilities;

import java.util.HashSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.stonecrusher.exception.ProcessCustomError;

@Service
public class SendPushNotification {

	public static final Logger logger = LogManager.getLogger(SendPushNotification.class);

	public String sendPushNotification() throws ProcessCustomError
	{
		ResponseEntity<String> response = null;
		
		try {			
			   // Take server key and other details(Put these details in the constant)
			   String androidFcmKey="AIzaSyBP3v81nDHd6vB82Y9XoIYmMiko6ZApOoA";
			   String androidFcmUrl="https://fcm.googleapis.com/fcm/send";
			   String deviceToken="cW8SrEYbvrM:APA91bElRxFGmbXcwk3vHyDwMspUiE_kM771Km250_Yq0QHuUcWR1l8Quk1uUT-4tNiFOkYEVPNdYW2ZwbEFFqlHQyQDhmU9hmY05c5qISRaqN1jTL60VbYuqAhrZ7-bt7rRm8kSiloL";

			   
			   RestTemplate restTemplate = new RestTemplate();
			   HttpHeaders httpHeaders = new HttpHeaders();

			   // Add server key in the headers	
			   httpHeaders.set("Authorization", "key=" + androidFcmKey);
			   httpHeaders.set("Content-Type", "application/json");
			   
			   // Put title and other details
			   JSONObject msg = new JSONObject();
			   JSONObject json = new JSONObject();			   
			   msg.put("title", "StoneCrusher Alert");
			   msg.put("notification ", " Stone Crusher App");
			   msg.put("priority","high");
			   HashSet<String> ids = new HashSet<String>();
			   ids.add(deviceToken);
			   
			   // Put data in the message
			   json.put("data", msg);
			   json.put("registration_ids", ids);

			   HttpEntity<String> httpEntity = new HttpEntity<String>(json.toString(),httpHeaders);
			   response = restTemplate.exchange(androidFcmUrl, HttpMethod.POST, httpEntity,String.class);
			   
			    // Check response and display message accordingly
				if (response.getStatusCode() == HttpStatus.OK) {
						logger.info("Sent successfully" + "RESPONSE" + response.getBody());
				} else {
						logger.error("Sent failed" + "RESPONSE" + response.getBody());
				}				

			} catch (JSONException e) {
			   e.printStackTrace();
			   throw new ProcessCustomError("Issue while sending the notification ", HttpStatus.INTERNAL_SERVER_ERROR);
			}

		// return the response	
		return response.getBody();
	}
}
