package com.stonecrusher.common;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;

public class ApiError implements Serializable{
	
	private static final long serialVersionUID = -1122688221544728777L;
	
	private int code;

	private String errorNo;
	
	private String message;
	
	// Its for internal use only
	private String stack;
		
	public ApiError()
	{
		
	}

	public ApiError(HttpStatus code, String message) {
		this.code = code.value();
		this.message = message;
	}

	public ApiError(int code, String errorNo, String message, String stack) {
		super();
		this.code = code;
		this.errorNo = errorNo;
		this.message = message;
		this.stack = stack;
	}

	public ApiError(int code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public String getErrorNo() {
		return errorNo;
	}

	public void setErrorNo(String errorNo) {
		this.errorNo = errorNo;
	}

	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStack() {
		return stack;
	}

	public void setStack(String stack) {
		this.stack = stack;
	}

	public String toString()
	{
		return "ApiError [ code = "+ code + ", message = " + message + ", errorno " + errorNo + ", stack " + stack +" ] " ;
	}
	
	public String toJsonString() throws JSONException {
		
		final JSONObject obj = new JSONObject();
		obj.put("code", this.code);
		obj.put("errorNo", this.errorNo);
		obj.put("message", this.message);
		obj.put("stack", this.stack);
		
		return obj.toString();
	}
}
