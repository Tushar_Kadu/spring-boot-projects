package com.stonecrusher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@SpringBootApplication
@IntegrationComponentScan
public class StoneCrusherApplication {

	private static final Logger LOGGER = LogManager.getLogger(StoneCrusherApplication.class);
			
	public static void main(String[] args) {
		LOGGER.info(" Starting the application");
		SpringApplication.run(StoneCrusherApplication.class, args);
	}
	
}
