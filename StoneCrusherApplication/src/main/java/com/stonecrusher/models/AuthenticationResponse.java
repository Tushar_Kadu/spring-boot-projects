package com.stonecrusher.models;

/**
 * Class to hold the JWT token details
 * @author GS-1157
 *
 */
public class AuthenticationResponse {

	private final String jwtToken;
	private String status;
	
	public AuthenticationResponse(String token, String status)
	{
		this.jwtToken=token;
		this.status =status;
	}
	
	public String getJwt()
	{
		return this.jwtToken;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
