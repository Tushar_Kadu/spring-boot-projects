package com.stonecrusher.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="Temperature")
public class Temperature  {

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="sensor_id")
	int tempId;
	
	@Column(name="temp_sensor")
	String tempSensorName;
	
	@Column(name="temp_value")
	double tempvalue;
	
	@Column(name="temp_state")
	String state;
	
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "crusherstage_id", nullable = false)
    private CrushingStages crushingstage;

	public Temperature()
	{
		super();
	}
	
	public double getTempvalue() {
		return tempvalue;
	}

	public void setTempvalue(double tempvalue) {
		this.tempvalue = tempvalue;
	}

	public CrushingStages getCrushingstage() {
		return crushingstage;
	}

	public void setCrushingstage(CrushingStages crushingstage) {
		this.crushingstage = crushingstage;
	}
	
	public int getTempId() {
		return tempId;
	}

	public void setTempId(int tempId) {
		this.tempId = tempId;
	}

	public String getTempSensorName() {
		return tempSensorName;
	}

	public void setTempSensorName(String tempSensorName) {
		this.tempSensorName = tempSensorName;
	}

	public double getTemperature() {
		return tempvalue;
	}

	public void setTemperature(double temperature) {
		this.tempvalue = temperature;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}	
	
}
