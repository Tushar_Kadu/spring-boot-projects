package com.stonecrusher.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.stonecrusher.models.Role;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.JoinColumn;

/**
 * Class to hold the User details
 * @author GS-1157
 *
 */
@Entity
@Table(name = "my_user")
public class User {
	
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="user_id")
	int userId;
	
	@Column(name="user_name")
	String userName;
	
	@Column(name="user_pass")
	String password;

	@Column(name="user_conf_pass")
	String confirmpassword ;

	public User()
	{
		super();
	}

	public User(int userId, String userName, String password, String confirmpassword, String address, String firstName,
			String lastName, String phoneNumber, String email, String profilePic, String pushNotification,
			List<Role> roles) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.password = password;
		this.confirmpassword = confirmpassword;
		this.address = address;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.profilePic = profilePic;
		this.pushNotification = pushNotification;
		this.roles = roles;
	}

	@Column(name="user_add")
	String address;
	
	@Column(name="user_firstname")
	private String firstName;
	
	@Column(name="user_lastname")
	private String lastName;

	@Column(name="user_phonenumber")
	private String phoneNumber;

	@Column(name="user_email")
	private String email;

	@JsonIgnore
	@Column(name="user_profileimage")
	private String profilePic;
	
	@Column(name="user_pushNotification")	
	private String pushNotification;
	
	@ManyToMany(cascade=CascadeType.MERGE)
    @JoinTable(
    		 name="user_role",
    	       joinColumns={@JoinColumn(name="USER_ID", referencedColumnName="user_id")},
    	       inverseJoinColumns={@JoinColumn(name="ROLE_ID", referencedColumnName="role_Id")}
    		 )

    private List<Role> roles = new ArrayList<Role>();
		

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getConfirmpassword() {
		return confirmpassword;
	}

	public void setConfirmpassword(String confirmpassword) {
		this.confirmpassword = confirmpassword;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	public String getPushNotification() {
		return pushNotification;
	}

	public void setPushNotification(String pushNotification) {
		this.pushNotification = pushNotification;
	}


	public int getUserId() {
		return userId;
	}
	
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
			
	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	public void clearRoles()
	{
		this.roles = null;
	}

}
