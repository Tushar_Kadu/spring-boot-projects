package com.stonecrusher.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="Belt")
public class Belt {

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="belt_id")
	int beltId;
	
	@Column(name="belt_status")
	int isRunning;
	
	@Column(name="belt_name")
	String beltName;
	
	@Column(name="company_name")
	String companyName;

	@Column(name="belt_type")
	String type;

	@Column(name="belt_length")
	double length;
	
	@Column(name="belt_width")
	double width;
	
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "crusherstage_id", nullable = false)
    private CrushingStages crushingstage;

	public Belt() {
		super();
	}

	
	public Belt(int beltId, int isRunning, String beltName, String companyName, String type, double length,
			double width, CrushingStages crushingstage) {
		super();
		this.beltId = beltId;
		this.isRunning = isRunning;
		this.beltName = beltName;
		this.companyName = companyName;
		this.type = type;
		this.length = length;
		this.width = width;
		this.crushingstage = crushingstage;
	}


	public CrushingStages getCrushingstage() {
		return crushingstage;
	}

	public void setCrushingstage(CrushingStages crushingstage) {
		this.crushingstage = crushingstage;
	}

	public int getBeltId() {
		return beltId;
	}


	public void setBeltId(int beltId) {
		this.beltId = beltId;
	}


	public int getIsRunning() {
		return isRunning;
	}


	public void setIsRunning(int isRunning) {
		this.isRunning = isRunning;
	}


	public String getBeltName() {
		return beltName;
	}


	public void setBeltName(String beltName) {
		this.beltName = beltName;
	}


	public String getCompanyName() {
		return companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public double getLength() {
		return length;
	}


	public void setLength(double length) {
		this.length = length;
	}


	public double getWidth() {
		return width;
	}


	public void setWidth(double width) {
		this.width = width;
	}

}
