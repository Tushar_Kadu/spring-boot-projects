package com.stonecrusher.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="CrushingStages")
public class CrushingStages {

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="stage_id")
	int stageId;
	
	@Column(name="stage_name")
	String stageName;	

	/**
	 * There is one to one relationship between CrushingStages and other Models(Crusher,Metal,Temperature,Belt)
	 */	
	@OneToOne(fetch = FetchType.LAZY,
            cascade =  CascadeType.ALL,
            mappedBy = "crushingstage")
	
	private Crusher crusher;

	@OneToOne(fetch = FetchType.LAZY,
            cascade =  CascadeType.ALL,
            mappedBy = "crushingstage")
	
	private Belt belt;
	
	@OneToOne(fetch = FetchType.LAZY,
            cascade =  CascadeType.ALL,
            mappedBy = "crushingstage")
	
	private Metal metal;

	@OneToOne(fetch = FetchType.LAZY,
            cascade =  CascadeType.ALL,
            mappedBy = "crushingstage")
	
	private Temperature temperature;
	
	@OneToOne(fetch = FetchType.LAZY,
            cascade =  CascadeType.ALL,
            mappedBy = "crushingstage")
	
	private Load load;

	
	public CrushingStages() {
		
	}

	public CrushingStages(String stageName) {
		super();
		this.stageName = stageName;
	}

	public int getStageId() {
		return stageId;
	}

	public void setStageId(int stageId) {
		this.stageId = stageId;
	}

	public String getStageName() {
		return stageName;
	}
	
	public Crusher getCrusher() {
		return crusher;
	}

	public void setCrusher(Crusher crusher) {
		this.crusher = crusher;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	public Belt getBelt() {
		return belt;
	}

	public void setBelt(Belt belt) {
		this.belt = belt;
	}

	public Metal getMetal() {
		return metal;
	}

	public void setMetal(Metal metal) {
		this.metal = metal;
	}

	public Temperature getTemperature() {
		return temperature;
	}

	public void setTemperature(Temperature temperature) {
		this.temperature = temperature;
	}

	public Load getLoad() {
		return load;
	}

	public void setLoad(Load load) {
		this.load = load;
	}

}
