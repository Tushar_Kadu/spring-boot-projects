package com.stonecrusher.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="Metal")
public class Metal {

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="metal_id")
	int metalId;
	
	@Column(name="metal_name")
	String metalName;
	
	@Column(name="company_name")
	String companyName;

	@Column(name="metal_type")
	String type;

	@Column(name="metalStatus")
	int hasMetal;
	
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "crusherstage_id", nullable = false)
    private CrushingStages crushingstage;

	public Metal(int metalId, String metalName, String companyName, String type, int hasMetal,
			CrushingStages crushingstage) {
		super();
		this.metalId = metalId;
		this.metalName = metalName;
		this.companyName = companyName;
		this.type = type;
		this.hasMetal = hasMetal;
		this.crushingstage = crushingstage;
	}

	public Metal()
	{
	}
	
	public CrushingStages getCrushingstage() {
		return crushingstage;
	}

	public void setCrushingstage(CrushingStages crushingstage) {
		this.crushingstage = crushingstage;
	}
	
	public int getHasMetal() {
		return hasMetal;
	}

	public void setHasMetal(int hasMetal) {
		this.hasMetal = hasMetal;
	}
	
	public int getMetalId() {
		return metalId;
	}

	public void setMetalId(int metalId) {
		this.metalId = metalId;
	}

	public String getMetalName() {
		return metalName;
	}

	public void setMetalName(String metalName) {
		this.metalName = metalName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}	
}
