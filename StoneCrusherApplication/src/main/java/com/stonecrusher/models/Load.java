package com.stonecrusher.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="Load")
public class Load {

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	int id;

	@Column(name="load_state")
	String state;
	
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "crusherstage_id", nullable = false)
    private CrushingStages crushingstage;

	public Load()
	{
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public CrushingStages getCrushingstage() {
		return crushingstage;
	}

	public void setCrushingstage(CrushingStages crushingstage) {
		this.crushingstage = crushingstage;
	}
}