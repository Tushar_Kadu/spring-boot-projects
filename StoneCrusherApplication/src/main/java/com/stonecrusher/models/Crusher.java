package com.stonecrusher.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="Crusher")
public class Crusher {

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="crusher_id")
	int crusherId;
	
	@Column(name="crusher_name")
	String crusherName;
	
	@Column(name="company_name")
	String companyName;

	@Column(name="crusher_status")
	int isRunning;
	
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "crusherstage_id", nullable = false)
    private CrushingStages crushingstage;

	public Crusher() {
		super();
	}
	
	public CrushingStages getCrushingstage() {
		return crushingstage;
	}

	public void setCrushingstage(CrushingStages crushingstage) {
		this.crushingstage = crushingstage;
	}

	public int getIsRunning() {
		return isRunning;
	}

	public void setIsRunning(int isRunning) {
		this.isRunning = isRunning;
	}

	public int getCrusherId() {
		return crusherId;
	}

	public void setCrusherId(int crusherId) {
		this.crusherId = crusherId;
	}

	public String getCrusherName() {
		return crusherName;
	}

	public void setCrusherName(String crusherName) {
		this.crusherName = crusherName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

}
