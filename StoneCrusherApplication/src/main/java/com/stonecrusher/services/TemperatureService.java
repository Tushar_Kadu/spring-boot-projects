package com.stonecrusher.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stonecrusher.models.CrushingStages;
import com.stonecrusher.models.Temperature;
import com.stonecrusher.repository.CrushingStagesRepository;
import com.stonecrusher.repository.TemperatureRepository;

@Service
public class TemperatureService {

	@Autowired
	TemperatureRepository temperatureRepository;
	
	@Autowired
	CrushingStagesRepository crushingStagesRepository;	
	
	public Temperature saveTemperature(Temperature temp)
	{
		// Get crushing stage from the crusher and save its id		
		CrushingStages stage = crushingStagesRepository.findByStageName(temp.getCrushingstage().getStageName());

		// Set existing stage 
		if(null != stage)
			temp.setCrushingstage(stage);

		return temperatureRepository.save(temp);
	}
	
	public Temperature findTemperature(String tempSensorName)
	{
		return temperatureRepository.findByTempSensorName(tempSensorName);
	}
	
	public Temperature updateTemperatureValue(String stageName, double tempValue)
	{
		// Find crushing stage by name	
		CrushingStages stage = crushingStagesRepository.findByStageName(stageName);

		// Find temp sensor object
		Temperature tempSensor = stage.getTemperature();
		
		// Update temperature value
		tempSensor.setTemperature(tempValue);
		
		// Set temperature state according to temp value
		if(tempValue < 100)
			tempSensor.setState("G"); // Green state

		if(tempValue > 100 && tempValue < 130)
			tempSensor.setState("O"); // Orange state
		
		if(tempValue > 100 && tempValue < 130)
			tempSensor.setState("R"); // Red state
				
		return temperatureRepository.save(tempSensor);
	}
	
	public Temperature updateTemperatureState(String stageName, String state)
	{
		// Find crushing stage by name	
		CrushingStages stage = crushingStagesRepository.findByStageName(stageName);

		// Find temp sensor object
		Temperature tempSensor = stage.getTemperature();
		
		tempSensor.setState(state); // G/O/R state

		return temperatureRepository.save(tempSensor);
	}
}
