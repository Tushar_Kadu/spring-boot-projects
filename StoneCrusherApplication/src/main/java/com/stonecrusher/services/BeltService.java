package com.stonecrusher.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stonecrusher.models.CrushingStages;
import com.stonecrusher.models.Belt;
import com.stonecrusher.repository.BeltRepository;
import com.stonecrusher.repository.CrushingStagesRepository;

@Service
public class BeltService {

	@Autowired
	BeltRepository beltRepository;
	
	@Autowired
	CrushingStagesRepository crushingStagesRepository;	
	

	public Belt saveBelt( Belt belt)
	{
		// Get crushing stage from the crusher and save its id		
		CrushingStages stage = crushingStagesRepository.findByStageName(belt.getCrushingstage().getStageName());
		
		// Set existing stage 
		if(null != stage)
			belt.setCrushingstage(stage);
			

		return beltRepository.save(belt);
	}
	
	public Belt findBelt(String name)
	{
		return beltRepository.findByBeltName(name);
	}
	
	public List<Belt> findAllBelts()
	{
		List<Belt> belts = new ArrayList<Belt>();
		Iterable<Belt> itr = beltRepository.findAll();
		itr.forEach(belt -> belts.add(belt));
			
		return belts;
	}
	
	
	
	/**
	 * Update belt isrunning status
	 * @param stageName
	 * @param isRunning
	 * @return
	 */
	public Belt updateBeltRunningStatus(String stageName, int isRunning)
	{
		// Find crushing stage by name	
		CrushingStages stage = crushingStagesRepository.findByStageName(stageName);
		
		// Get belt details from
		Belt belt = stage.getBelt();
		
		// update belt running status
		belt.setIsRunning(isRunning);
		
		// return belt object		
		return beltRepository.save(belt);
	}
}
