package com.stonecrusher.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stonecrusher.models.CrushingStages;
import com.stonecrusher.models.Metal;
import com.stonecrusher.repository.CrushingStagesRepository;
import com.stonecrusher.repository.MetalRepository;

@Service
public class MetalService {

	@Autowired
	MetalRepository metalRepository;
	
	@Autowired
	CrushingStagesRepository crushingStagesRepository;	
	

	public Metal saveMetal( Metal metal)
	{
		// Get crushing stage from the crusher and save its id		
		CrushingStages stage = crushingStagesRepository.findByStageName(metal.getCrushingstage().getStageName());
		
		// Set existing stage 
		if(null != stage)
			metal.setCrushingstage(stage);
		
		// Save metal details in the crushing stage
		stage.setMetal(metal);
		crushingStagesRepository.save(stage);
		
		return metalRepository.save(metal);
	}
	
	public Metal findMetal(String name)
	{
		return metalRepository.findByMetalName(name);
	}
	
	/**
	 * Update metal status
	 * @param stageName = metal name
	 * @param hasMetal = has metal details or not 
	 * @return
	 */
	public Metal updateHasMetaldetails(String stageName, int hasMetal)
	{
		// Find crushing stage by name	
		CrushingStages stage = crushingStagesRepository.findByStageName(stageName);
		
		// Get metal details from
		Metal metal = stage.getMetal();
		
		// Update metal state
		metal.setHasMetal(hasMetal);
		
		// Save metal details
		return metalRepository.save(metal);
	}
}
