package com.stonecrusher.services;

import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stonecrusher.controllers.UserController;

@Service
public class MQTTService {

	private static final Logger LOGGER = LogManager.getLogger(UserController.class);
	
	@Autowired
	CrusherService crusherservice;
	
	@Autowired
	MetalService metalService;
		
	@Autowired
	TemperatureService tempservice;
	
	@Autowired
	BeltService beltservice;
	
	@Autowired
	CrushingStagesService crushingstageservice;
	
	@Autowired	
	LoadService loadservice;
	
	public void processMQTTMessage(String message) throws JSONException
	{
		LOGGER.info(" [MQTTMessageService] MQTT message received " + message);

		// As we need to update models like crusher, load,Belt, Temperature we need to check its JSON onstance 
		// Sample input is 
/*			{
			"stage1": {
				"crusher": "0",
				"belt": "0",
				"metal": "0",
				"temperature": "g",
				"load": "g"
			},
			"stage2": {
				"crusher": "0",
				"belt": " 0",
				"temperature": "r",
				"load": "o"
			},
			"stage3": {
				"crusher": "r",
				"belt": "0",
				"temperature": "g",
				"load": "g"
			}
		}
		*/

		
		// Convert JSON string to the JSON object
		JSONObject crusherStagingDetails = new JSONObject(message.trim());	
		
		// Iterate on each stage
		Iterator<String> keys = crusherStagingDetails.keys();
		
		while(keys.hasNext()) {
			
			String key = keys.next();
						
			if (crusherStagingDetails.get(key) instanceof JSONObject) {
				
				// Call update API 			
				updateDetails(crusherStagingDetails,key);
			}
		}
				
	}

	/**
	 * Update attributes of model (Belt,load,Temperature..)
	 * @param crusherStagingDetails
	 * @param key
	 * @throws JSONException
	 */
	private void updateDetails(JSONObject crusherStagingDetails, String key) throws JSONException {
		
		JSONObject stageDetails = (JSONObject) crusherStagingDetails.get(key);
		String crusher_stage = key;
		String model = "";
		
		// Check that crushing stage is exist or not
		if(null != crushingstageservice.findCrushingStage(crusher_stage))
		{
			Iterator<String> models = stageDetails.keys();
			
			System.out.println("Stage details are " + stageDetails.toString());
			
			while(models.hasNext())
			{
				model = models.next();
				
				// TODO :- Create ENUM from hardcoded values
				if(model.equals("crusher"))
				{
					LOGGER.info(" Calling API to update the Crusher details Crusher, Value is " +stageDetails.getInt(model));

					// Call API to update crusher state		
					crusherservice.updateCrusherRunningState(crusher_stage, stageDetails.getInt(model));
				}

				if(model.equals("temperature"))
				{
					LOGGER.info(" Calling API to update the Temperature details, Value is " +stageDetails.getString(model));

					// Call API to update temperature state		
					tempservice.updateTemperatureState(crusher_stage, stageDetails.getString(model));

				}

				if(model.equals("belt"))
				{
					LOGGER.info(" Calling API to update the Belt details, value is "+stageDetails.getInt(model));

					// Call API to update belt state		
					beltservice.updateBeltRunningStatus(crusher_stage, stageDetails.getInt(model));
					
				}

				if(model.equals("load"))
				{
					LOGGER.info(" Calling API to update the Load details, Value is "+stageDetails.getString(model));
					// Call API to update temperature state		
					loadservice.updateLoadingState(crusher_stage, stageDetails.getString(model));

				}

				if(model.equals("metal"))
				{
					LOGGER.info(" Calling API to update the Metal details, value is "+stageDetails.getInt(model));
					
					// Call API to update metal state
					metalService.updateHasMetaldetails(crusher_stage, stageDetails.getInt(model));					
				}

			}
		}
		else
		{
			LOGGER.warn(" Crusher Stage " + crusher_stage + " does not exist");
		}
	}
}
