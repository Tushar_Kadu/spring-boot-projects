package com.stonecrusher.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stonecrusher.models.CrushingStages;
import com.stonecrusher.repository.CrushingStagesRepository;

@Service
public class CrushingStagesService {

	@Autowired
	CrushingStagesRepository crushingStagesRepository;
	
	public CrushingStages saveCrushingStage(CrushingStages stage)
	{
		return crushingStagesRepository.save(stage);
	}
	
	public CrushingStages findCrushingStage(String stageName)
	{
		return crushingStagesRepository.findByStageName(stageName);
	}
	
	public List<CrushingStages> findAllCrushingStages()
	{
		List<CrushingStages> stages = new ArrayList<>();		
		crushingStagesRepository.findAll().forEach(stages::add); 		
		return stages;
	}
	
}
