package com.stonecrusher.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stonecrusher.models.Role;
import com.stonecrusher.repository.RoleRepository;

@Service
public class RoleService {
	
	@Autowired
	RoleRepository roleRepository;

	/** 
	 *  Add role details
	 * @param role
	 * @return
	 */
	public Role addRole(Role role) {
		
		return roleRepository.save(role);
	}
	
	/**
	 * Find role by its name 
	 * @param roleName
	 * @return
	 */
	public Role findRoleByName(String roleName) {
		
		return roleRepository.findByRoleName(roleName);
		
	}

}
