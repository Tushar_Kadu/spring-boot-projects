package com.stonecrusher.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stonecrusher.models.Role;
import com.stonecrusher.models.User;
import com.stonecrusher.repository.RoleRepository;
import com.stonecrusher.repository.UserRepository;
import com.stonecrusher.request.ResetPasswordRequest;


@Service
public class UserService implements UserDetailsService{

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	

	
	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		 
		User user = userRepository.findByUserName(userName);
		
		// Use core user for the authentication and other details
		org.springframework.security.core.userdetails.User coreUser = new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(), getAuthorities(user));
		//org.springframework.security.core.userdetails.User coreUser = new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(), new ArrayList<>());
		return coreUser;
	}
	
	/**
	 * Get all the authorities of the users (like ADMIN/USER)
	 * @param user
	 * @return
	 */
	private static Collection<? extends GrantedAuthority> getAuthorities(User user) {
        String[] userRoles = user.getRoles().stream().map((role) -> role.getName()).toArray(String[]::new);
        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(userRoles);
        return authorities;
    }
	
	public @ResponseBody List<User> getAllUsers(){
		
		List<User> users = new ArrayList<>();
		userRepository.findAll().forEach(users::add);		
		return users;
	}
	
	/**
	 * Add new user
	 * 	1. Check that Role already exist or not
	 *  2. Create User with Role mapping
	 * @param user
	 * @return
	 * @throws Exception 
	 */
	public User addUser(User user) throws Exception {
					
		// We dont need to create new role every time if role is already exist we need to use existing reference
		List<Role> userRoles = user.getRoles();
		
		// Iterate on roles and set existing roleIds
		for (Role role : userRoles) {
			
			Role existingRole = roleRepository.findByRoleName(role.getName());
			
			// If ROLE already exist use that
			if(null != existingRole)
				role.setRoleId(existingRole.getRoleId());
		}
		
		// Save user details
		return userRepository.save(user);
	}
	
	public Optional<User> findUser(int userId) {
		
		return userRepository.findById(userId);
		
	}

	public User findByUserName(String userName) {
		
		return userRepository.findByUserName(userName);
		
	}

	public User resetPassword(ResetPasswordRequest passwordChangeReq) {
		// TODO Auto-generated method stub
		User user = userRepository.findByUserName(passwordChangeReq.getUserName());
		
		// Set new password 
		user.setPassword(passwordChangeReq.getNewPassword());
		
		// Update user in the repository
		return userRepository.save(user);
				
	}

}
