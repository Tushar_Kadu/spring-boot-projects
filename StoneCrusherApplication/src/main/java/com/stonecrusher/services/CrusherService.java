package com.stonecrusher.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stonecrusher.models.Crusher;
import com.stonecrusher.models.CrushingStages;
import com.stonecrusher.repository.CrusherRepository;
import com.stonecrusher.repository.CrushingStagesRepository;

@Service
public class CrusherService {

	@Autowired
	CrusherRepository crusherRepository;

	@Autowired
	CrushingStagesRepository crushingStagesRepository;

	public Crusher saveCrusher(Crusher crusher)
	{
		// Get crushing stage from the crusher and save its id		
		CrushingStages stage = crushingStagesRepository.findByStageName(crusher.getCrushingstage().getStageName());
		
		// Set existing stage 
		if(null != stage)
			crusher.setCrushingstage(stage);
		
		return crusherRepository.save(crusher);
	}
	
	public Crusher findCrusher(String crusherName)
	{
		return crusherRepository.findByCrusherName(crusherName);
	}
	
	
	public Crusher updateCrusherRunningState(String stageName, int isRunning)
	{
		// Find crushing stage by name	
		CrushingStages stage = crushingStagesRepository.findByStageName(stageName);

		// Find crusher by its name
		Crusher crusher = stage.getCrusher();
		
		// Update running state of crusher
		crusher.setIsRunning(isRunning);
	
		return crusherRepository.save(crusher);
	}
	
}
