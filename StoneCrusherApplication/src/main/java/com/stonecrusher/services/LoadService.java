package com.stonecrusher.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stonecrusher.models.CrushingStages;
import com.stonecrusher.models.Load;
import com.stonecrusher.repository.CrushingStagesRepository;
import com.stonecrusher.repository.LoadRepository;

@Service
public class LoadService {
	
	@Autowired
	LoadRepository loadRepository;

	@Autowired
	CrushingStagesRepository crushingStagesRepository;
	
	public Load saveLoad(Load load)
	{
		// Get crushing stage from the crusher and save its id		
		CrushingStages stage = crushingStagesRepository.findByStageName(load.getCrushingstage().getStageName());
		
		// Set existing stage 
		if(null != stage)
			load.setCrushingstage(stage);
		
		// Save metal details in the crushing stage
		stage.setLoad(load);
		
		crushingStagesRepository.save(stage);
		
		return loadRepository.save(load);
	}
		
	public Load updateLoadingState(String stageName, String loadingState)
	{		
		// Find crushing stage by name	
		CrushingStages stage = crushingStagesRepository.findByStageName(stageName);

		// Get load state object
		Load load = stage.getLoad();
		
		// Set loading stage
		load.setState(loadingState);
		
		// Save load object
		return loadRepository.save(load);
	}
}
