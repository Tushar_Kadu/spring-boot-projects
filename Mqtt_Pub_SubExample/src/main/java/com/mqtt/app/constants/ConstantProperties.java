package com.mqtt.app.constants;

public interface ConstantProperties {

	 String mqttBrokerUrl = "tcp://localhost:1883";
	 String mqttBrokerUserName = "guest";
	 String mqttBrokerPassword = "guest";
	 String mqttTopicName = "iot_data";
}
