package com.mqtt.app.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;


@Service
public class MqttMessageService {

	private static final Logger LOGGER = LogManager.getLogger(MqttMessageService.class);
	public void processMQTTMessage(String message)
	{
		LOGGER.info(" [MQTTMessageService] MQTT message received " + message);

	}
}
