package com.mqtt.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1")
public class MqttPublisher {

	@Autowired
    private ApplicationContext ctx;
	
	@RequestMapping(method=RequestMethod.POST, value="/publishMQTTMessage")
	public ResponseEntity<?> publishMqttMessage(@RequestBody String mqttMsg)
	{	
		// Get MQTT gateway object from the application context to send the message
		MQTTGateway gw = ctx.getBean(MQTTGateway.class);
		
		// Send message on the Topic		
		gw.sendToMqtt(mqttMsg);
		
		// Return response
		return new ResponseEntity<>(HttpStatus.OK);		
	}

	// This is the interface which is referring MQTT configuration channel
	@MessagingGateway(defaultRequestChannel = "mqttOutboundChannel")
    public interface MQTTGateway {
		
		// Send msg to the queue
        void sendToMqtt(String data);

    }

}
