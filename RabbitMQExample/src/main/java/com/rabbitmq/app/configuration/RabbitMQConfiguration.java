package com.rabbitmq.app.configuration;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfiguration {

	// Queue which we are going to use 
	
	@Value("${rabbitmq.queue}")	
	private String queueName;
	
	// Exchange name
	@Value("${rabbitmq.exchange}")
	private String exchange;
	
	// routing key (Will not use this in case of fanout exchange)
	@Value("${rabbitmq.routingkey}")
	private String routingKey;
		
	@Bean
	Queue createQueue() {
		
		// You dont need to create queue externally in the RabbitMQ
		// It wil create it dynamically
		return new Queue(queueName, true); // False = means it will delete that key once rabbitMQ restarted
										   // True = pass true for durability 
				
		//It will be redeclared if the broker stops and is restarted while the connection factory is alive, but all messages will be lost
	}
	
	@Bean
	FanoutExchange createExchenge() {
		
		return new FanoutExchange(exchange);
	}
	
	@Bean
	Binding createQueueExchangeBinding(Queue queue, FanoutExchange exchange)
	{
		return BindingBuilder.bind(queue).to(exchange);		
		// Uncomment following code if you want to use any other exchage like DirectExchange
		//return BindingBuilder.bind(queue).to(exchange).with(routingkey);
	}
}
