package com.rabbitmq.app.services;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RabbitMQPublisherService {

	@Autowired
	private AmqpTemplate rabbitTemplate;
	
	@Value("${rabbitmq.exchange}")
	private String exchange;
	
	@Value("${rabbitmq.routingkey}")
	private String routingkey;	
	
	
	// Publish message on the rabbitMQ
	public void publishMessage(String message)
	{
		System.out.println("Sending message " + message + " to RabbitMQ");
		rabbitTemplate.convertAndSend(exchange,"",message);
		System.out.println("Msg sent to RabbitMQ");
	}
	
}
