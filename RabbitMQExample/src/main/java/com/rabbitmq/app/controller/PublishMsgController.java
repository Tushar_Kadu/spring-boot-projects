package com.rabbitmq.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rabbitmq.app.services.RabbitMQPublisherService;


@RestController
public class PublishMsgController {
	
	@Autowired
	RabbitMQPublisherService publisherService;

	@RequestMapping(method=RequestMethod.POST, value="/PublishMsg")
	public ResponseEntity<?> pubishMessage(@RequestBody String message)
	{	
		// Call RabbitMQ service to send message
		publisherService.publishMessage(message);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
