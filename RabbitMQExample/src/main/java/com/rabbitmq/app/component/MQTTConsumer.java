package com.rabbitmq.app.component;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class MQTTConsumer {

	@RabbitListener(queues = "${rabbitmq.queue}")
	public void receiveMessage(String message){
	
		System.out.println("Message Recived from the RabbitMQ");
		System.out.println(message);
		System.out.println("DONE");
	}
}
